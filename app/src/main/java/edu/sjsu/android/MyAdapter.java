package edu.sjsu.android;
import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import
        android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private List<String> names;
    private int[] images = new int[5];

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView text;
        public ImageView img;
        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            text = (TextView) v.findViewById(R.id.text);
            img = (ImageView) v.findViewById(R.id.icon);
        }
    }

    public MyAdapter(List<String> nameList, int[] imagesList)
    {
        this.names = nameList;
        this.images = imagesList;
    }
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        final String name = names.get(position);
        holder.text.setText(name);
        holder.img.setImageResource(images[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                /*switch (position) {
                    case (0):
                    {
                        //Log.d("CS 175","Panda");
                        MainActivity.chosen = 0;
                    }

                }*/
                MainActivity.chosen = position;
                final Context context = v.getContext();
                if (position!=4) {
                    Intent intent = new Intent(context, AnimalDetail.class);
                    intent.putExtra("chosen", Integer.toString(position));
                    context.startActivity(intent);
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("WARNING!");
                    builder.setMessage("This animal is very scary, and you should proceed with caution. Do you wish to continue?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(context, AnimalDetail.class);
                                    intent.putExtra("chosen", Integer.toString(position));
                                    context.startActivity(intent);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    builder.create();
                    builder.show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }
}
