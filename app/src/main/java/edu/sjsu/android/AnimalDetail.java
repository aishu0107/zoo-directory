package edu.sjsu.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalDetail extends AppCompatActivity {

    android.widget.TextView textView;
    android.widget.ImageView imgView;
    android.widget.TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_detail);
        textView = (TextView)findViewById(R.id.textView3);
        imgView = (ImageView)findViewById(R.id.imageView);
        textView2 = (TextView)findViewById(R.id.textView4);
        Bundle myInput = this.getIntent().getExtras();
        String val = myInput.getString("chosen");
        int chosen = Integer.parseInt(val);
        switch(chosen)
        {
            case 0:
                textView.setText("Zebra");
                imgView.setImageResource(R.drawable.big_zebra);
                textView2.setText("This is a cute little zebra named Marty.");
                break;
            case 1:
                textView.setText("Panda");
                imgView.setImageResource(R.drawable.big_panda);
                textView2.setText("This is a chubby panda named Po.");
                break;
            case 2:
                textView.setText("Fox");
                imgView.setImageResource(R.drawable.big_fox);
                textView2.setText("This is a curious red fox named Nick Wilde.");
                break;
            case 3:
                textView.setText("Lion");
                imgView.setImageResource(R.drawable.big_lion);
                textView2.setText("This is a ferocious little lion named Leo");
                break;
            case 4:
                textView.setText("Tiger");
                imgView.setImageResource(R.drawable.big_tiger);
                textView2.setText("This is a powerful little tiger named Sher Khan");
                break;
        }

    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case (R.id.information):
                Intent myIntent = new Intent(this, ZooInfoActivity.class);
                startActivity(myIntent);
                break;
            case (R.id.uninstall):
                Intent del = new Intent(Intent.ACTION_DELETE);
                del.setData(Uri.parse("package:edu.sjsu.android"));
                startActivity(del);

        }
        return true;
    }

}