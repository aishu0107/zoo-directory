package edu.sjsu.android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    List<String> names;
    int[] images = new int[5];
    public static int chosen = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        names = new ArrayList<>();
        names.add("Zebra");
        names.add("Panda");
        names.add("Fox");
        names.add("Lion");
        names.add("Tiger");
        images[0] = R.drawable.small_zebra;
        images[1] = R.drawable.small_panda;
        images[2] = R.drawable.small_fox;
        images[3] = R.drawable.small_lion;
        images[4] = R.drawable.small_tiger;
        mAdapter = new MyAdapter(names,images);
        recyclerView.setAdapter(mAdapter);
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case (R.id.information):
                Intent myIntent = new Intent(this, ZooInfoActivity.class);
                startActivity(myIntent);
                break;
            case (R.id.uninstall):
                /*Intent del = new Intent(Intent.ACTION_DELETE);
                del.setData(Uri.parse("package:edu.sjsu.android"));
                startActivity(del);*/
                Intent del = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(del);


        }
        return true;
    }


}