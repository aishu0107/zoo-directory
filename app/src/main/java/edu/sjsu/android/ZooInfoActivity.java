package edu.sjsu.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ZooInfoActivity extends MainActivity implements android.view.View.OnClickListener {
    android.widget.Button button;
    String num = "tel:888-8888";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoo_info);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
    }
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case (R.id.button):
                Intent call = new Intent(Intent.ACTION_DIAL, Uri.parse(num));
                startActivity(call);
        }
    }
}